# srcdir

Manage your `~/src` automatically, by querying your accounts with popular git services.

Maintains a simple directory structure: `<domain>/<repo>`. On all currently supported providers, `repo` consists of the user or org name, followed by a slash and the repo name.

By default, srcdir clones all repos you own, have starred, or belong to orgs (/groups/workspaces/etc) you are a part of. This is configurable on a per-account basis.

When adding an account, srcdir prompts the user for a token. This could be the user's password, although most platforms support creating "app tokens" (and some require it).

Currently only cloning by SSH is supported.

## Services supported:

| Name | Token Type |
|------|------------|
| Github | Password (for non-2FA setups) or [Access Token](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token) |
| Gitlab | [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) only |
| Bitbucket | [Personal access token guide](https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html) only |

## Usage

```
srcdir help
srcdir help config
srcdir help config init
srcdir config init --root ~/non-default-src-path

srcdir help config add
srcdir config add --kind github --user example
srcdir config add --kind gitlab --user example --domain custom-gitlab.example.com --favorites=false
srcdir config add --kind gitlab --user another-user --orgs=example-org,example-org2

srcdir clone
```

## Installation

```
go build
sudo cp srcdir /usr/local/bin
```

## Todo

- CI-built binary releases
- Clone arbitrary repos (parse url, determine domain and repo path from it) Current workflow: star the repo in your account, `srcdir clone`.
- Pull command (git fetch all repos, fast forward checked out branch only if safe)
- Oauth support (unclear if possible)
- More providers (gitea)
