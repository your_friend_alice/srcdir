module gitlab.com/your_friend_alice/srcdir

go 1.14

require (
	github.com/acomagu/bufpipe v1.0.3 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/chzyer/test v1.0.0 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/flynn/go-shlex v0.0.0-20150515145356-3f9db97f8568 // indirect
	github.com/go-git/go-git/v5 v5.11.0
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/jessevdk/go-flags v1.5.0 // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/whilp/git-urls v1.0.0
	golang.org/x/crypto v0.16.0
	gopkg.in/yaml.v3 v3.0.1
)
