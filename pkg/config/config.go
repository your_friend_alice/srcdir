package config

import (
	"fmt"
	"gitlab.com/your_friend_alice/srcdir/pkg/services"
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
	"strings"
)

type Config struct {
	Root     string              `yaml:"root"`
	Accounts []*services.Account `yaml:"accounts"`
}

func (c Config) Save() error {
	path, err := ConfigPath()
	if err != nil {
		return err
	}
	tmpPath := path + ".tmp"

	// open file
	file, err := os.OpenFile(tmpPath, os.O_CREATE|os.O_WRONLY, 0o600)
	defer file.Close()
	if err != nil {
		return fmt.Errorf("Error opening temp config file %q:\n  %w", tmpPath, err)
	}
	err = file.Truncate(0)
	if err != nil {
		return fmt.Errorf("Error truncating temp config file %q:\n  %w", tmpPath, err)
	}
	encoder := yaml.NewEncoder(file)
	err = encoder.Encode(c)
	if err != nil {
		return fmt.Errorf("Error encoding config to temp file %q:\n  %w", tmpPath, err)
	}
	encoder.Close()
	err = os.Rename(tmpPath, path)
	if err != nil {
		return fmt.Errorf("Error moving temp config file %q to %q:\n  %w", tmpPath, path, err)
	}
	return nil
}

func (c Config) ValidateUniqueness() error {
	accountSet := map[string]struct{}{}
	for _, account := range c.Accounts {
		_, taken := accountSet[account.String()]
		if taken {
			return fmt.Errorf("Duplicate account %q in config", account.String())
		}
		accountSet[account.String()] = struct{}{}
	}
	return nil
}

func (c Config) Validate() error {
	err := c.ValidateUniqueness()
	if err != nil {
		return err
	}
	// check account validity
	for _, account := range c.Accounts {
		err := account.Validate()
		if err != nil {
			return err
		}
	}
	return nil
}

var DefaultConfig = Config{
	Root:     "~/src",
	Accounts: []*services.Account{},
}

func ConfigPath() (string, error) {
	dir, err := os.UserConfigDir()
	if err != nil {
		return "", fmt.Errorf("Error getting config path:\n  %w", err)
	}
	return filepath.Join(dir, "srcdir.yaml"), nil
}

func Load() (Config, error) {
	c := Config{}
	// get path
	path, err := ConfigPath()
	if err != nil {
		return c, err
	}

	// open file
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		return c, fmt.Errorf("Error opening config file %q:\n  %w", path, err)
	}

	// decode
	decoder := yaml.NewDecoder(file)
	decoder.KnownFields(true)
	err = decoder.Decode(&c)
	if err != nil {
		return c, fmt.Errorf("Error decoding config file %q:\n  %w", path, err)
	}

	// expand ~ in root
	c.Root, err = expandTilde(c.Root)
	if err != nil {
		return c, fmt.Errorf("Error expanding root path from config file %q:\n  %w", path, err)
	}

	// fill defaults
	if len(c.Root) == 0 {
		c.Root = DefaultConfig.Root
	}
	for _, account := range c.Accounts {
		err := account.Init()
		if err != nil {
			return c, fmt.Errorf("Error applying defaults to config file %q:\n  %w", path, err)
		}
	}
	return c, nil
}

var sep = string(filepath.Separator)

func expandTilde(path string) (string, error) {
	path = strings.TrimSuffix(path, sep)
	if filepath.IsAbs(path) {
		return path, nil
	}
	home, err := os.UserHomeDir()
	if err != nil {
		return "", err
	}
	path = strings.TrimPrefix(path, "~")
	path = strings.TrimPrefix(path, "/")
	return filepath.Join(home, path), nil

}
