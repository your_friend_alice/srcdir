package git

import (
	"fmt"
	git "github.com/go-git/go-git/v5"
	gitSsh "github.com/go-git/go-git/v5/plumbing/transport/ssh"
	url "github.com/whilp/git-urls"
	"gitlab.com/your_friend_alice/srcdir/pkg/services"
	"golang.org/x/crypto/ssh"
	"path/filepath"
	"strings"
)

func CloneAll(root string, repos []services.Repo, parallelism uint) error {
	err := checkHosts(repos)
	if err != nil {
		return fmt.Errorf("Error checking SSH host keys:\n  %w", err)
	}

	errs := make(chan error)
	jobs := make(chan services.Repo)
	// create workers
	for i := uint(0); i < parallelism; i++ {
		go func() {
			for job := range jobs {
				errs <- clone(root, job)
			}
		}()
	}
	// feed workers
	go func() {
		for _, repo := range repos {
			fmt.Printf("Cloning %q...\n", repo.Url)
			jobs <- repo
		}
	}()
	// consume errors
	for _ = range repos {
		err := <-errs
		if err != nil {
			return err
		}
	}
	return nil
}

func clone(root string, repo services.Repo) error {
	_, err := git.PlainClone(
		filepath.Join(root, repo.Path),
		false,
		&git.CloneOptions{
			URL: repo.Url,
		},
	)
	if err != nil && err != git.ErrRepositoryAlreadyExists {
		return fmt.Errorf("Error cloning repo %q:\n %w", repo.Url, err)
	} else {
		return nil
	}
}

func checkHosts(repos []services.Repo) error {
	hosts, err := getSshHosts(repos)
	if err != nil {
		return err
	}
	callback, err := gitSsh.NewKnownHostsCallback()
	if err != nil {
		return err
	}
	for _, host := range hosts {
		// TODO: check if host key is in known_hosts. If not, prompt user
		_ = host
		err = checkHostKey(host, callback)
		if err != nil {
			return err
		}
	}
	return nil
}

type sshHost struct {
	User string
	Host string
}

func (h sshHost) String() string {
	return h.User + "@" + h.Host
}

func getSshHosts(repos []services.Repo) ([]sshHost, error) {
	hosts := map[sshHost]struct{}{}
	for _, repo := range repos {
		u, err := url.Parse(repo.Url)
		if err != nil {
			return []sshHost{}, fmt.Errorf("Error parsing SSH URL %q:\n  %w", repo.Url, err)
		}
		host := u.Hostname()
		host += ":"
		if len(u.Port()) == 0 {
			host += "22"
		} else {
			host += u.Port()
		}
		hosts[sshHost{u.User.Username(), host}] = struct{}{}
	}
	out := make([]sshHost, 0, len(hosts))
	for host := range hosts {
		out = append(out, host)
	}
	return out, nil
}

func checkHostKey(host sshHost, callback ssh.HostKeyCallback) error {
	config := &ssh.ClientConfig{
		User:            host.User,
		Auth:            []ssh.AuthMethod{},
		HostKeyCallback: callback,
	}
	client, err := ssh.Dial("tcp", host.Host, config)
	if client != nil {
		client.Close()
	}
	// that's right, golang.org/x/crypto/ssh doesn't type its errors.
	if err != nil && strings.HasPrefix(err.Error(), "ssh: handshake failed: ssh: unable to authenticate,") {
		return nil
	}
	return fmt.Errorf("Error checking host key for %q:\n  %w", host, err)
}
