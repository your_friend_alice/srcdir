package services

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path/filepath"
)

func bbReq(account *Account, url string, out interface{}) error {
	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	request.SetBasicAuth(account.User, account.Token)
	response, err := client.Do(request)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	if response.StatusCode != 200 {
		return fmt.Errorf("Server returned status code %s for url %s", response.Status, url)
	}
	return json.NewDecoder(response.Body).Decode(out)
}

func bbRepos(account *Account, path string) ([]Repo, error) {
	url := account.ApiUrl + path
	out := []Repo{}
	for len(url) != 0 {
		var err error
		resp := bbRepoResponse{}
		err = bbReq(account, url, &resp)
		url = resp.Next
		if err != nil {
			return []Repo{}, err
		}
		for _, repo := range resp.Values {
			ssh, err := repo.url()
			if err != nil {
				return []Repo{}, fmt.Errorf("Error listing repos for account %q:\n  %w", account.String(), err)
			}
			out = append(out, Repo{
				Url:  ssh,
				Path: filepath.Join(account.Domain, repo.Name),
			})
		}
	}
	return out, nil
}

type bbWorkspaceResponse struct {
	Values []bbWorkspace
	Next   string
}
type bbWorkspace struct{ Slug string }

type bbRepoResponse struct {
	Values []bbRepo
	Next   string
}
type bbRepo struct {
	Links struct {
		Clone []bbLink
	}
	Name string `json:"full_name"`
}

func (r bbRepo) url() (string, error) {
	out := ""
	for _, link := range r.Links.Clone {
		if link.Name == "ssh" {
			out = link.Href
		}
	}
	if len(out) == 0 {
		return "", fmt.Errorf("Repo %q has no ssh URL", r.Name)
	}
	return out, nil
}

type bbLink struct {
	Name string
	Href string
}

type bbUser struct {
	Username string
}

func init() {
	Add("bitbucket", Service{
		DefaultDomain: "bitbucket.org",

		ApiUrl: func(domain string) string {
			if domain == "bitbucket.org" {
				return "https://api.bitbucket.org/2.0"
			} else {
				return "https://" + domain + "/rest/api/2.0"
			}
		},

		Validate: func(account *Account) error {
			user := bbUser{}
			err := bbReq(account, account.ApiUrl+"/user", &user)
			fmt.Println(user)
			return err
		},

		Orgs: func(account *Account) ([]string, error) {
			out := []string{}
			url := account.ApiUrl + "/workspaces"
			for len(url) > 0 {
				var err error
				resp := bbWorkspaceResponse{}
				err = bbReq(account, url, &resp)
				url = resp.Next
				if err != nil {
					return []string{}, err
				}
				for _, workspace := range resp.Values {
					if len(workspace.Slug) > 0 {
						out = append(out, workspace.Slug)
					}
				}
			}
			return out, nil
		},

		OwnRepos: func(account *Account) ([]Repo, error) {
			return bbRepos(account, "/repositories/"+account.User)
		},

		OrgRepos: func(org string, account *Account) ([]Repo, error) {
			return bbRepos(account, "/repositories/"+org)
		},

		FavoriteRepos: func(account *Account) ([]Repo, error) {
			// Bitbucket doesn't appear to have a "favorites" mechanism
			return []Repo{}, nil
		},
	})
}
