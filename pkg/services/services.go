package services

import (
	"fmt"
)

var services = map[string]Service{}

func Add(name string, service Service) {
	_, alreadyExists := services[name]
	if alreadyExists {
		panic(fmt.Errorf("Service %q already added", name))
	}
	services[name] = service
}

type Repo struct {
	Url  string
	Path string
}

type Account struct {
	Kind      string
	User      string
	Token     string
	Domain    string
	ApiUrl    string
	Favorites *bool     `yaml:"favorites,omitempty"`
	Personal  *bool     `yaml:"personal,omitempty"`
	Orgs      *[]string `yaml:"orgs,omitempty"`
}

func (a *Account) String() string {
	return a.Domain + "/" + a.User
}

func (a *Account) getService() (Service, error) {
	service, ok := services[a.Kind]
	if ok {
		return service, nil
	} else {
		kinds := make([]string, 0, len(services))
		for kind := range services {
			kinds = append(kinds, kind)
		}
		return Service{}, fmt.Errorf("Invalid service kind: %q. Supported services: %v", a.Kind, kinds)
	}
}

func (a *Account) Init() error {
	service, err := a.getService()
	if err != nil {
		return err
	}
	if len(a.Domain) == 0 {
		a.Domain = service.DefaultDomain
	}
	if len(a.ApiUrl) == 0 {
		a.ApiUrl = service.ApiUrl(a.Domain)
	}
	if a.Favorites == nil {
		temp := true
		a.Favorites = &temp
	}
	if a.Personal == nil {
		temp := true
		a.Personal = &temp
	}
	return nil
}

func (a *Account) Validate() error {
	service, err := a.getService()
	if err != nil {
		return err
	}
	err = service.Validate(a)
	if err != nil {
		return fmt.Errorf("Error validating account %q:\n  %w", a.String(), err)
	}
	return nil
}

func (a *Account) GetOrgs() ([]string, error) {
	if a.Orgs == nil || len(*a.Orgs) == 1 && (*a.Orgs)[0] == "*" {
		service, err := a.getService()
		if err != nil {
			return []string{}, fmt.Errorf("Error getting orgs for Account %q:\n  %w", a.String(), err)
		}
		return service.Orgs(a)
	} else {
		return *a.Orgs, nil
	}
}

func (a *Account) GetOwnRepos() ([]Repo, error) {
	if !*a.Personal {
		return []Repo{}, nil
	}
	service, err := a.getService()
	if err != nil {
		return []Repo{}, fmt.Errorf("Error getting personal repos for Account %q:\n  %w", a.String(), err)
	}
	repos, err := service.OwnRepos(a)
	if err != nil {
		return repos, err
	}
	return repos, nil
}

func (a *Account) GetOrgRepos() ([]Repo, error) {
	repos := []Repo{}
	orgs, err := a.GetOrgs()
	if err != nil {
		return repos, err
	}
	service, err := a.getService()
	if err != nil {
		return repos, fmt.Errorf("Error getting org repos for Account %q:\n  %w", a.String(), err)
	}
	for _, org := range orgs {
		result, err := service.OrgRepos(org, a)
		if err != nil {
			return repos, err
		}
		repos = append(repos, result...)
	}
	return repos, nil
}

func (a *Account) GetFavoriteRepos() ([]Repo, error) {
	if !*a.Favorites {
		return []Repo{}, nil
	}
	service, err := a.getService()
	if err != nil {
		return []Repo{}, fmt.Errorf("Error getting Favorite repos for Account %q:\n  %w", a.String(), err)
	}
	repos, err := service.FavoriteRepos(a)
	if err != nil {
		return repos, err
	}
	return repos, nil
}

type nothing struct{}
type RepoSet map[Repo]nothing

func (r RepoSet) AddFromSet(repos RepoSet) {
	for repo := range repos {
		r[repo] = nothing{}
	}
}
func (r RepoSet) AddFromList(repos []Repo) {
	for _, repo := range repos {
		r[repo] = nothing{}
	}
}
func (r RepoSet) All() []Repo {
	out := make([]Repo, 0, len(r))
	for k := range r {
		out = append(out, k)
	}
	return out
}

func (a *Account) GetRepos() (RepoSet, error) {
	repos := RepoSet{}

	result, err := a.GetOwnRepos()
	if err != nil {
		return repos, err
	}
	repos.AddFromList(result)

	result, err = a.GetOrgRepos()
	if err != nil {
		return repos, err
	}
	repos.AddFromList(result)

	result, err = a.GetFavoriteRepos()
	if err != nil {
		return repos, err
	}
	repos.AddFromList(result)

	return repos, nil
}

type Service struct {
	DefaultDomain string
	ApiUrl        func(domain string) string
	Validate      func(account *Account) error
	Orgs          func(account *Account) ([]string, error)
	OwnRepos      func(account *Account) ([]Repo, error)
	OrgRepos      func(org string, account *Account) ([]Repo, error)
	FavoriteRepos func(account *Account) ([]Repo, error)
}
