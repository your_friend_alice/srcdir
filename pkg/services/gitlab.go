package services

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"path/filepath"
)

func gitlabReq(account *Account, url string, out interface{}, strict bool) (string, error) {
	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	request.Header.Add("Authorization", "Bearer "+account.Token)
	response, err := client.Do(request)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	if response.StatusCode != 200 {
		return "", fmt.Errorf("Server returned status code %s for url %s", response.Status, url)
	}
	decoder := json.NewDecoder(response.Body)
	if strict {
		decoder.DisallowUnknownFields()
	}
	return nextPage(response), decoder.Decode(out)
}

func gitlabRepos(account *Account, path string) ([]Repo, error) {
	url := account.ApiUrl + path
	out := []Repo{}
	for len(url) != 0 {
		var err error
		repos := []gitlabRepo{}
		url, err = gitlabReq(account, url, &repos, false)
		if err != nil {
			return []Repo{}, err
		}
		for _, repo := range repos {
			out = append(out, Repo{
				Url:  repo.Url,
				Path: filepath.Join(account.Domain, repo.Name)})
		}
	}
	return out, nil
}

type gitlabUser struct {
	Name string `json:"username"`
}
type gitlabOrg struct {
	Name string `json:"path"`
}
type gitlabRepo struct {
	Url  string `json:"ssh_url_to_repo"`
	Name string `json:"path_with_namespace"`
}

func init() {
	Add("gitlab", Service{
		DefaultDomain: "gitlab.com",

		ApiUrl: func(domain string) string {
			return fmt.Sprintf("https://" + domain + "/api/v4")
		},

		Validate: func(account *Account) error {
			user := gitlabUser{}
			_, err := gitlabReq(account, account.ApiUrl+"/user", &user, false)
			if err != nil {
				return err
			}
			if user.Name != account.User {
				return fmt.Errorf("Token is for user %q, not the given user %q", user.Name, account.User)
			}
			return nil
		},

		Orgs: func(account *Account) ([]string, error) {
			out := []string{}
			url := account.ApiUrl + "/groups?min_access_level=10"
			for len(url) > 0 {
				var err error
				orgs := []gitlabOrg{}
				url, err = gitlabReq(account, url, &orgs, false)
				if err != nil {
					return []string{}, err
				}
				for _, org := range orgs {
					out = append(out, org.Name)
				}
			}
			return out, nil
		},

		OwnRepos: func(account *Account) ([]Repo, error) {
			return gitlabRepos(account, "/users/"+url.PathEscape(account.User)+"/projects")
		},

		OrgRepos: func(org string, account *Account) ([]Repo, error) {
			return gitlabRepos(account, "/groups/"+org+"/projects")
		},

		FavoriteRepos: func(account *Account) ([]Repo, error) {
			return gitlabRepos(account, "/users/"+url.PathEscape(account.User)+"/starred_projects")
		},
	})
}
