package services

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path/filepath"
)

func githubReq(account *Account, url string, out interface{}, strict bool) (string, error) {
	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	request.SetBasicAuth(account.User, account.Token)
	response, err := client.Do(request)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	if response.StatusCode != 200 {
		return "", fmt.Errorf("Server returned status code %s for url %s", response.Status, url)
	}
	decoder := json.NewDecoder(response.Body)
	if strict {
		decoder.DisallowUnknownFields()
	}
	return nextPage(response), decoder.Decode(out)
}

func githubRepos(account *Account, path string) ([]Repo, error) {
	url := account.ApiUrl + path
	out := []Repo{}
	for len(url) != 0 {
		var err error
		repos := []githubRepo{}
		url, err = githubReq(account, url, &repos, false)
		if err != nil {
			return []Repo{}, err
		}
		for _, repo := range repos {
			out = append(out, Repo{
				Url:  repo.Url,
				Path: filepath.Join(account.Domain, repo.Name)})
		}
	}
	return out, nil
}

type githubOrg struct{ Login string }
type githubRepo struct {
	Url  string `json:"ssh_url"`
	Name string `json:"full_name"`
}

func init() {
	Add("github", Service{
		DefaultDomain: "github.com",

		ApiUrl: func(domain string) string {
			if domain == "github.com" {
				return "https://api.github.com"
			} else {
				return "https://" + domain + "/api/v3"
			}
		},

		Validate: func(account *Account) error {
			user := githubOrg{}
			_, err := githubReq(account, account.ApiUrl+"/user", &user, false)
			return err
		},

		Orgs: func(account *Account) ([]string, error) {
			out := []string{}
			url := account.ApiUrl + "/user/orgs"
			for len(url) > 0 {
				var err error
				orgs := []githubOrg{}
				url, err = githubReq(account, url, &orgs, false)
				if err != nil {
					return []string{}, err
				}
				for _, org := range orgs {
					if len(org.Login) > 0 {
						out = append(out, org.Login)
					}
				}
			}
			return out, nil
		},

		OwnRepos: func(account *Account) ([]Repo, error) {
			return githubRepos(account, "/user/repos")
		},

		OrgRepos: func(org string, account *Account) ([]Repo, error) {
			return githubRepos(account, fmt.Sprintf("/orgs/%s/repos", org))
		},

		FavoriteRepos: func(account *Account) ([]Repo, error) {
			return githubRepos(account, "/user/starred")
		},
	})
}
