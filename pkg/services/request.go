package services

import (
	"net/http"
	"regexp"
)

func nextPage(response *http.Response) string {
	links, ok := response.Header["Link"]
	if ok && len(links) == 1 {
		matches := linkRe.FindAllStringSubmatch(links[0], -1)
		for _, match := range matches {
			if match[2] == "next" {
				return match[1]
			}
		}
	}
	return ""
}

var linkRe *regexp.Regexp

func init() {
	linkRe = regexp.MustCompile("<(.+?)>; rel=\"(.+?)\"")
}
