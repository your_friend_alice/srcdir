package cmd

import (
	"fmt"
	"github.com/chzyer/readline"
	"github.com/spf13/cobra"
	"gitlab.com/your_friend_alice/srcdir/pkg/config"
	"gitlab.com/your_friend_alice/srcdir/pkg/services"
	"os"
	"strings"
)

var configFlags struct {
	Root      string
	Kind      string
	User      string
	Domain    string
	ApiUrl    string
	Favorites bool
	Personal  bool
	Orgs      []string
	ShowToken bool
}

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Configure srcdir",
	Args:  cobra.NoArgs,
	RunE:  func(cmd *cobra.Command, args []string) error { return nil },
}

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialize srcdir with an empty config file",
	Args:  cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		path, err := config.ConfigPath()
		if err != nil {
			return err
		}
		_, err = os.Stat(path)
		if err == nil || !os.IsNotExist(err) {
			return fmt.Errorf("Config file %q already exists", path)
		}
		conf := config.DefaultConfig
		conf.Root = configFlags.Root
		return conf.Save()
	},
}

var pathCmd = &cobra.Command{
	Use:   "path",
	Short: "Print the path to the srcdir config file",
	Args:  cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		path, err := config.ConfigPath()
		if err != nil {
			return err
		}
		fmt.Println(path)
		return nil
	},
}

var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Add an account",
	Args:  cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		conf, err := config.Load()
		if err != nil {
			return err
		}
		account := services.Account{
			Kind:      configFlags.Kind,
			User:      configFlags.User,
			Domain:    configFlags.Domain,
			ApiUrl:    configFlags.ApiUrl,
			Favorites: &configFlags.Favorites,
			Personal:  &configFlags.Personal,
			Orgs:      &configFlags.Orgs,
		}
		err = account.Init()
		if err != nil {
			return err
		}
		account.Token, err = promptForToken(configFlags.ShowToken)
		if err != nil {
			return err
		}
		if err != nil {
			return err
		}
		conf.Accounts = append(conf.Accounts, &account)
		err = conf.ValidateUniqueness()
		if err != nil {
			return err
		}
		return conf.Save()
	},
}

var tokenPrompt = "Access Token: "

func promptForToken(show bool) (string, error) {
	var token string
	var err error
	if show {
		token, err = readline.Line(tokenPrompt)
	} else {
		var b []byte
		b, err = readline.Password(tokenPrompt)
		token = string(b)
	}
	return strings.TrimSpace(token), err
}

var lsCmd = &cobra.Command{
	Use:   "ls",
	Short: "List acounts",
	Args:  cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		conf, err := config.Load()
		if err != nil {
			return err
		}
		for _, account := range conf.Accounts {
			fmt.Println(account.String())
		}
		return nil
	},
}

var rmCmd = &cobra.Command{
	Use:   "rm",
	Short: "Remove an account",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		conf, err := config.Load()
		accounts := make([]*services.Account, 0, len(conf.Accounts))
		if err != nil {
			return err
		}
		removed := 0
		for _, account := range conf.Accounts {
			if account.String() == args[0] {
				removed++
			} else {
				accounts = append(accounts, account)
			}
		}
		if removed == 0 {
			return fmt.Errorf("Account %q not found", args[0])
		}
		conf.Accounts = accounts
		return conf.Save()
	},
}

func init() {
	rootCmd.AddCommand(configCmd)

	addCmd.Flags().StringVarP(&configFlags.Kind, "kind", "k", "", "Kind of provider")
	addCmd.MarkFlagRequired("kind")
	addCmd.Flags().StringVarP(&configFlags.User, "user", "u", "", "User name")
	addCmd.MarkFlagRequired("user")
	addCmd.Flags().StringVarP(&configFlags.Domain, "domain", "d", "", "Domain, if different from the default for the given provider")
	addCmd.Flags().StringVarP(&configFlags.ApiUrl, "api", "a", "", "API URL, if different from the default API URL for the given provider/domain")
	addCmd.Flags().BoolVarP(&configFlags.Favorites, "favorites", "f", true, "Clone all starred/favorited repositories")
	addCmd.Flags().BoolVarP(&configFlags.Personal, "personal", "p", true, "Clone the user's own repositories")
	addCmd.Flags().StringSliceVarP(&configFlags.Orgs, "orgs", "o", []string{"*"}, "List of organizations/groups to fetch. A single * means all the user is a member of")
	addCmd.Flags().BoolVarP(&configFlags.ShowToken, "show-token", "s", false, "When prompting for the token, show what's being typed")
	configCmd.AddCommand(addCmd)

	configCmd.AddCommand(lsCmd)

	initCmd.Flags().StringVarP(&configFlags.Root, "root", "r", config.DefaultConfig.Root, "Root directory of the source tree")
	configCmd.AddCommand(initCmd)

	configCmd.AddCommand(pathCmd)

	configCmd.AddCommand(rmCmd)
}
