package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/your_friend_alice/srcdir/pkg/config"
	"gitlab.com/your_friend_alice/srcdir/pkg/git"
	"gitlab.com/your_friend_alice/srcdir/pkg/services"
)

var cloneCmd = &cobra.Command{
	Use:   "clone",
	Short: "Clone from all configured accounts",
	Args:  cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		conf, err := config.Load()
		if err != nil {
			return err
		}
		err = conf.Validate()
		if err != nil {
			return err
		}
		repos := services.RepoSet{}
		for _, account := range conf.Accounts {
			fmt.Printf("Getting Repos for Account %q...\n", account)
			result, err := account.GetRepos()
			if err != nil {
				return err
			}
			repos.AddFromSet(result)
		}
		return git.CloneAll(conf.Root, repos.All(), Parallelism)
	},
}

func init() {
	rootCmd.AddCommand(cloneCmd)
}
