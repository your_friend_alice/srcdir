package cmd

import (
	"github.com/spf13/cobra"
	"os"
)

var Parallelism uint

var rootCmd = &cobra.Command{
	Use:          "srcdir",
	Short:        "Manage your ~/src directory using your git hosting accounts",
	Args:         cobra.NoArgs,
	SilenceUsage: true,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().UintVarP(&Parallelism, "jobs", "j", 8, "Number of git tasks to perform in parallel")
}
